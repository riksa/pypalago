import logging
import random
import pypalago
from pypalago.map.Tile import Tile
from pypalago.player.Player import Player

__author__ = 'riksa'

logger = logging.getLogger('palago.player')


class RandomPlayer(Player):
    def __init__(self, white):
        Player.__init__(self, white)

    def __repr__(self):
        return u'%s [%s]' % (Player.__repr__(self), self.__class__.__name__)

    def get_moves(self, palago_map):
        # move 1
        logger.debug(" 1st move free tiles %s", palago_map.free_tiles)
        tile = random.choice(palago_map.free_tiles)
        move1 = Tile(tile[0], tile[1], random.randrange(3))
        logger.debug(" 1st move choice %s", move1)

        free_tiles = palago_map.free_tiles_with(move1)
        logger.debug(" 2nd move free tiles %s", free_tiles)
        tile = random.choice(free_tiles)
        move2 = Tile(tile[0], tile[1], random.randrange(3))
        logger.debug(" 2nd move choice %s", move2)

        return [move1, move2]


