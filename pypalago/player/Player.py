__author__ = 'riksa'

class Player():
    def __init__(self, white):
        self.white = white

    def __repr__(self):
        return u'%s' % (u'WHITE' if self.white else u'BLACK')
