from math import cos, sin, tan

__author__ = 'riksa'


def key(x, y):
    return (x, y)


class Tile():
    def __init__(self, x, y, orientation):
        self.x = x
        self.y = y
        self.orientation = orientation

    def key(self):
        return key(self.x, self.y)

    def neighbours(self):
        return [
            key(self.x, self.y + 1),
            key(self.x + 1, self.y),
            key(self.x + 1, self.y - 1),
            key(self.x, self.y - 1),
            key(self.x - 1, self.y),
            key(self.x - 1, self.y + 1),
        ]

    def __repr__(self):
        return u'[%d,%d], orientation: %d' % (self.x, self.y, self.orientation)

    def cartesian(self, scale):
        return (self.x + 0.5*self.y ) * scale, (self.y * 0.5*1.73) * scale
