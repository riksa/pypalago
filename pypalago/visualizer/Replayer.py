# -*- coding: utf-8

import logging
from math import sin, sqrt
import pygame

__author__ = 'riksa'

logger = logging.getLogger('palago.visualizer')


class Replayer():
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((self.width, self.height))
        self.tile_sprites = pygame.sprite.Group()

    def replay(self, replay):
        logger.debug(u'Replay of %s vs %s' % (replay.players[0], replay.players[1]))
        tile_sprites = pygame.sprite.Group()

        minx = maxx = miny = maxy = 0
        for move in replay.moves:
            for tile in move:
                # 1 or 2 tiles in a move
                if tile.x < minx:
                    minx = tile.x
                if tile.y < miny:
                    miny = tile.y
                if tile.x > maxx:
                    maxx = tile.x
                if tile.y > maxy:
                    maxy = tile.y

        for move in replay.moves:
            for tile in move:
                logger.debug(u'Tile 1: %s' % tile)
                tile_sprites.add(TileSprite(tile, minx, miny, maxx, maxy))

        tile_sprites.update(self.width/2, self.height/2)
        logger.debug(replay.result())

        running = 1

        while running:
            event = pygame.event.poll()
            if event.type == pygame.QUIT:
                running = 0
            self.screen.fill((0, 0, 0))
            tile_sprites.draw(self.screen)
            pygame.display.flip()


class TileSprite(pygame.sprite.Sprite):
    """
    This class represents the tile.
    It derives from the "Sprite" class in Pygame.
    """
    transparent = [0, 0, 0]

    def __init__(self, move, minx, miny, maxx, maxy):
        # Call the parent class (Sprite) constructor
        pygame.sprite.Sprite.__init__(self)

        # Load the image
        self.image = pygame.image.load("tile.bmp").convert()


        self.width=64
        self.image = pygame.transform.scale(self.image, (self.width, (int)(self.width / sqrt(0.75))))
        # Set our transparent color
        self.image.set_colorkey(TileSprite.transparent)
        orig_rect = self.image.get_rect()
        self.image = pygame.transform.rotate(self.image, move.orientation * 120)
        rot_rect = orig_rect.copy()
        rot_rect.center = self.image.get_rect().center
        self.image = self.image.subsurface(rot_rect).copy()

        self.rect = self.image.get_rect()
        self.move = move

    def update(self, xoff, yoff):
        cartesian = self.move.cartesian(self.rect.width)
        logger.debug(u'Tile cartesian: [%.0f, %.0f]' % (cartesian[0], cartesian[1]))
        self.rect.center = (cartesian[0] + xoff, cartesian[1] + yoff)


