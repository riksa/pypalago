# -*- coding: utf-8

__author__ = 'riksa'


class Replay():
    def __init__(self, players):
        self.moves = []
        self.winner = None
        self.players = players

    def add_move(self, tiles):
        self.moves.append(tiles)

    def finish(self, winner):
        self.winner = winner

    def result(self):
        if self.winner:
            return u'Game won by %s' % self.winner
        return u'Game ends in a tie'
