# -*- coding: utf-8

import logging
import random
import sys
from pypalago.core import Replay
from pypalago.map import Tile
from pypalago.player.RandomPlayer import RandomPlayer
from pypalago.visualizer.Replayer import Replayer

__author__ = 'riksa'

import pygame

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)
logger = logging.getLogger('palago.game')


class PalagoMap():
    MAX_TURNS = 24
    # http://keekerdc.com/2011/03/hexagon-grids-coordinate-systems-and-distance-calculations/
    def __init__(self, (players)):
        self.turn = 1
        self.white_turn = True
        self.first_tile = True
        self.tiles = {}
        self.free_tiles = []
        self.winner = None
        self.replay = Replay(players)

    def free_tile(self, key):
        if not self.tiles.keys():
            return True
        return not key in self.tiles.keys()

    def occupied_tile(self, key):
        return not self.free_tile(key)

    def valid_move(self, white, x, y, orientation):
        if not white == self.white_turn:
            logger.error("invalid move, it's not %ss turn" % "WHITE" if white else "BLACK")
            return False

        if not self.free_tile(Tile.key(x, y)):
            logger.error("invalid move, there's already a tile in (%d, %d)" % (x, y))
            return False

        if orientation not in range(0, 3):
            logger.error("invalid move, orientation %s not 0..2" % orientation)
            return False


        # free_tiles is empty for first move, anything goes
        if not self.free_tiles:
            return True

        if not Tile.key(x, y) in self.free_tiles:
            logger.error("invalid move, tile is not free (%s)" % Tile.key(x, y))
            return False

        return True

    def free_tiles_with(self, tile):
        return list(set(self.free_tiles) & set(tile.neighbours()))

    def add_tiles(self, player, tiles):
        self.replay.add_move(tiles)
        self.add_tile(player, tiles[0])
        if tiles[1]:
            self.add_tile(player, tiles[1])

    def add_tile(self, player, tile):
        if not self.valid_move(player.white, tile.x, tile.y, tile.orientation):
            raise Exception("Invalid move for %s, %s" % (player, tile))

        self.tiles[tile.key()] = tile
        logger.debug("%s moved to %s" % (player, tile))

        # update free_tiles
        if self.free_tiles:
            self.free_tiles.remove(tile.key())

        logger.debug("neighbours of %s = %s" % (tile.key(), tile.neighbours()))
        free_neighbours = filter(self.free_tile, tile.neighbours())
        logger.debug("Free neighbours: %s" % free_neighbours)

        self.free_tiles = list(set(self.free_tiles) | set(free_neighbours))
        logger.debug("Free tiles: %s", self.free_tiles)

        if not self.first_tile:
            self.white_turn = not self.white_turn  # piece #2, switch player
            self.turn += 1

        self.first_tile = not self.first_tile  # flip between move #1 and move #2

        return tile

    def game_over(self):
        return False

    def result(self):
        if self.turn <= self.MAX_TURNS:
            return u'Game is on'
        if not self.winner:
            return u'Game ends in a tie'
        return u'Game won by %s' % self.winner
        pass


replayer = Replayer(640, 640)

players = [RandomPlayer(True), RandomPlayer(False)]
palago_map = PalagoMap(players)
logger.info("Players: %s vs %s" % (players[0], players[1]))

player = players[0]
logger.debug("Turn #%d, %s plays" % (1, player))
# you should always start with this move
palago_map.add_tiles(player, (Tile.Tile(0, 0, 0), Tile.Tile(0, 1, 1)))

for turn in range(2, PalagoMap.MAX_TURNS + 1):
    # flip flop
    player = players[1] if player.white else players[0]

    logger.debug(40 * "=")
    logger.debug("Turn #%d, %s plays" % (turn, player))
    logger.debug("Map: %s" % str(palago_map.tiles))
    logger.debug(40 * "=")

    moves = player.get_moves(palago_map)
    palago_map.add_tiles(player, moves)
    if palago_map.game_over():
        break

logger.debug(40 * "*")
logger.info(u'%s', palago_map.result())
logger.debug("Map: %s" % str(palago_map.tiles))

replayer.replay(palago_map.replay)



